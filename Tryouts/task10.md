# Password strength checker

# Requirement
Based on the criteria given below need to classify the password strength.

You guys would have noticed when creating a new online account. Based on the password you typing it will suggest you the strength.

Criteria

weak - only numbers or only alphabets
medium - contains both alphabets and numbers
Strong - contains alphabets, numbers and special characters


# concepts needed
string methods, variables and operators

# Points to note
- Get input password as string
- Based on the criteria, classify the password strength
- print the strength

# Example
Input = 2342344

Expected output = weak

## example 2

Input = asdf398#ksjd

Expected output = strong

