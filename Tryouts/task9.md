# Types of gym people

# Requirement
Based on the frequency of coming to GYM, you need to classify what type of people they are.

Hardcore - Comes to gym all day 
Regular - comes on specific days but regularly
Occasional - Maybe once a week 
Absentee - Zero days in gym

Input is given in string contains 0 or 1.
1 means present in gym
0 means absent in gym

The input string will have 30 or 31 values either 0 or 1

# concepts needed
string methods, assignement, conditional statements, operators

# Points to note
- Take the input string 
- count 1s in the string 
- Based on the count classify the person based on the above mentioned criteria (Hardcore, Regular, occasional, absentee)

# Example
inputstring="1010101010101010101010101010101"

Expected output = Regular

case 2
inputstring="1111111111111111111111111111111"

Expected output = Hardcore


Happy coding !

