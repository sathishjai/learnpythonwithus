fruits = {"apple", "orange", "lemon", "banana", "papaya"}

# remove

# print(fruits)
# fruits.remove("pineapple")
# print(fruits)


# discard

# fruits.discard("pineapple")
# print(fruits)

# pop

# value = fruits.pop()
# print(fruits, value)

# del

print(fruits)
# del fruits
# print(fruits)

# clear
#
fruits.clear()
print(fruits)
