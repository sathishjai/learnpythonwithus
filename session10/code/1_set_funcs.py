# # Set Declaration
#
# names = set()
# fruits = set()
# data = {'d1', 'd2', 'd3', 2, 3.4, True}
#
# # print(names, fruits, data)
#
#
# # Declaring it with {}
#
# # data2 = {} # set()
# # print(data2, type(data2))
#
# # Declaring it with mutable things.
#
#
# # data2 = { [1, 2, 3,], {1:2} }
# # print(data2)
#
#
# # Length
#
# print(len(data))
#
# # max, Min
# data = {1, 2, 3.4}
# print(max(data), min(data))
#
#
# # type
#
# print(type(dat))

# any

data = {1, '', True}
# print(any(data))

# all
data = {1, '', True}
print(all(data))

data2 = { [1, 2, 3,], {1:2} }
print(data2)
